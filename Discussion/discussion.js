//Query Operators
//allow for more flexible querying of data within MongoDB

db.inventory.insertMany([
	{
		"name": "Javascript for Beginners",
		"author": "James Doe",
		"price": 5000,
		"stocks": 50,
		"publisher": "JS Publishing House"
	},
	{
		"name": "HTML and CSS",
		"author": "John Thomas",
		"price": 2500,
		"stocks": 38,
		"publisher": "NY Publishers"
	},
	{
		"name": "Web Development Fundamentals",
		"author": "Noah Jimenez",
		"price": 3000,
		"stocks": 10,
		"publisher": "Big Idea Publishing House"
	},
	{
		"name": "Java Programming",
		"author": "David Michael",
		"price": 10000,
		"stocks": 100,
		"publisher": "JS Publishing House"
	}
]);


//Comparison Query Operators
//$gt/$gte operators
/*
	Syntax:
		db.collectionName.find({ field: { $gt: value }})
		db.collectionName.find({ field: { $gte: value }}) -less than or equal
*/

db.inventory.find ({
	"stocks": {
		$gt: 50
	}
});

db.inventory.find({
	"stocks": {
		$gte: 50
	}
});

//$lt/$lte operator

db.inventory.find({
	"stocks": {
		$lt: 50
	}
});

db.inventory.find({
	"stocks": {
		$lte: 50
	}
});

//$ne operator - not equal (!=)

db.inventory.find({
	"stocks": {
		$ne: 50
	}
});

//$eq operator = equals
db.inventory.find({
	"stocks": {
		$eq: 50
	}
});

//or

db.inventory.find({
	"stocks": 50
});

//$in operator - should match the values of the documents
/*
	Syntax:
		db.collectionName.find({ field: { $in: [value1, value2]} })
*/

db.inventory.find({
	"price": {
		$in: [10000, 5000, 2500]
	}
});

//Logical Query Operator
//$and operator
/*
	Syntax:
		db.collectionName.method({
			$and: [
				{fieldA : valueA},
				{fieldB : valueB}
			]
		})
*/

db.inventory.find({
	$and: [
		{ 
			"stocks": { $ne: 50}
		},
		{
			"price": {$ne: 5000}
		}
	]
});

//same output with

db.inventory.find({
	"stocks": {
		$ne: 50
	},
	"price": {
		$ne: 5000
	}
});

//$or operator
/*
	Syntax:

*/

db.inventory.find({
	$or: [
		{
			"name": "HTML and CSS"
		},
		{
			"publisher": "JS Publishing House"
		}
	]
});

db.inventory.find({
	$or: [
		{
			"author": "James Doe"
		},
		{
			"price": {
				$lte: 5000
			}
		}
	]
});

//Field Projection - will only select specific fields to show
/*
	Syntax:
		db.collectionName.method({criteria}, {field: 1}) - to include
		db.collectionName.method({criteria}, {field: 0}) - will not include

	Field inclusion and field exclusion cannot be used at the same time, unless you will exclude and only the id.
*/

//Inclusion
db.inventory.find(
	{
		"publisher": "JS Publishing House"
	},
	{
		"name": 1,
		"author": 1,
	}
);

//Exclussion - this will remove the field indicated on the result
db.inventory.find(
	{
		"author": "Noah Jimenez"
	},
	{
		"price": 0,
		"stocks": 0
	}
);

db.inventory.find(
	{
		"price": {$lte: 5000}
	},
	{
		"_id": 0,
		"name": 1,
		"author": 1
	}
);

//Evaluation Query Operator
//$regex operator
/*
	Syntax:
		db.collectionName.method({field: {$regex: 'pattern', $options: 'optionsValue'}})
*/

//Case sensitive Query
db.inventory.find({
	"author": {
		$regex: 'M'
	}
});

//Case insensitive Query - $i will use to disregard the case of the value
db.inventory.find({
	"author": {
		$regex: 'M',
		$options: '$i'
	}
});