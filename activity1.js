db.users.insertMany([
	{
		"firstName": "Diane",
		"lastName": "Murphy",
		"email": "dmurphy@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Mary",
		"lastName": "Patterson",
		"email": "mpatterson@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Jeff",
		"lastName": "Firrelli",
		"email": "jfirrelli@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Gerald",
		"lastName": "Bondur",
		"email": "gbondur@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Pamela",
		"lastName": "Castillo",
		"email": "pcastillo@mail.com",
		"isAdmin": true,
		"isActive": false
	},
	{
		"firstName": "George",
		"lastName": "Vanauf",
		"email": "gvanauf@mail.com",
		"isAdmin": true,
		"isActive": true
	}
]);

db.courses.insertMany([
	{
		"name": "Professional Development",
		"price": 10000.0
	},
	{
		"name": "Business Processing",
		"price": 13000.0
	}
]);

db.users.find({"isAdmin": false});

db.courses.updateMany(
	{
		"name": "Professional Development"
	},
	{
		$set: {
			"enrollees": [ObjectId("620cc7ae7f7c311744f4784b"), ObjectId("620cc7ae7f7c311744f4784c"), ObjectId("620cc7ae7f7c311744f4784d"), ObjectId("620cc7ae7f7c311744f4784e")]
		}
	}
)